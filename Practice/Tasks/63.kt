import kotlin.math.max

fun maxSum(arr:IntArray):Int{
    var max1=0
    var max2=0
    for (a:Int in arr){
        max1 = max(max1+a,0)
        max2 = max(max2, max1)
    }
    return max2
}

fun main(){
    val tor = intArrayOf(-1, 10, -9, 5, 6, -10)
    println("${maxSum(tor)}")
}