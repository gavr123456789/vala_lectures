public enum BinSide{
    Left,
    Right
}
public class BinaryTree{
 
    public long? Data { get; private set; }
    public BinaryTree Left { get; set; }
    public BinaryTree Right { get; set; }
    public BinaryTree Parent { get; set; }

    public void In(long data){
        if (Data == null || Data == data){
            Data = data;
            return;
        }
        if (Data > data){
            if (Left == null) Left = new BinaryTree();
            Insert_withLongData(data, Left, this);
        }
        else{
            if (Right == null) Right = new BinaryTree();
            Insert_withLongData(data, Right, this);
        }
    }
 
    /* 
     Вставляет значение в определённый узел дерева
     data - Значение
     node - Целевой узел для вставки
     parent - Родительский узел*/
    private void Insert_withLongData(long data, BinaryTree node, BinaryTree parent){
    
        if (node.Data == null || node.Data == data){
            node.Data = data;
            node.Parent = parent;
            return;
        }
        if (node.Data > data){
            if (node.Left == null) node.Left = new BinaryTree();
            Insert_withLongData(data, node.Left, node);
        }
        else{
            if (node.Right == null) node.Right = new BinaryTree();
            Insert_withLongData(data, node.Right, node);
        }
    }
    /* Уставляет узел в определённый узел дерева
    // data - Вставляемый узел
    // node - Целевой узел
    parent - Родительский узел*/ 
    private void Insert_withData(BinaryTree data, BinaryTree node, BinaryTree parent)
    {
 
        if (node.Data == null || node.Data == data.Data){
            node.Data = data.Data;
            node.Left = data.Left;
            node.Right = data.Right;
            node.Parent = parent;
            return;
        }
        if (node.Data > data.Data){
            if (node.Left == null) node.Left = new BinaryTree();
            Insert_withData(data, node.Left, node);
        }
        else{
            if (node.Right == null) node.Right = new BinaryTree();
            Insert_withData(data, node.Right, node);
        }
    }

    // private BinSide? MeForParent(BinaryTree node)
    // {
    //     if (node.Parent == null) return null;
    //     if (node.Parent.Left == node) return BinSide.Left;
    //     if (node.Parent.Right == node) return BinSide.Right;
    //     return null;
    // }
 


    // public BinaryTree Find(long data)
    // {
    //     if (Data == data) return this;
    //     if (Data > data)
    //     {
    //         return Find_withNode(data, Left);
    //     }
    //     return Find_withNode(data, Right);
    // }

    // /*
    // data - Значение для поиска
    // node - Узел для поиска
    // */
    // public BinaryTree Find_withNode(long data, BinaryTree node)
    // {
    //     if (node == null) return null;
 
  ////        if (node.Data == data) return node;
    //     if (node.Data > data)
    //     {
    //         return Find_withNode(data, node.Left);
    //     }
    //     return Find_withNode(data, node.Right);
    // }
      
    // //Количество элементов в дереве
    // public long CountElements()
    // {
    //     return CountElements_withNode(this);
    // }
    // /* Количество элементов в определённом узле
    // node - Узел для подсчета
    // */ 
    // private long CountElements_withNode(BinaryTree node)
    // {
    //     long count = 1;
    //     if (node.Right != null)
    //     {
    //         count += CountElements_withNode( node.Right);
    //     }
    //     if (node.Left != null)
    //     {
    //         count+=CountElements_withNode( node.Left);
    //     }
    //     return count;
    // }
}
 
public class BinaryTreeExtensions
{
    public static void Print(BinaryTree node, bool Task){
        if (node != null){
            if (node.Parent == null){
                print(@"ROOT: $(node.Data)\n");
            }
            else{
                if (node.Parent.Left == node){
                    print(@"Left for $(node.Parent.Data)  --> $(node.Data)\n");
                }
                if(Task==false)
                if (node.Parent.Right == node){
                    print(@"Right for $(node.Parent.Data) --> $(node.Data)\n");
                }
            }
            if (node.Left != null){
                Print(node.Left,(Task==false) ? false : true );
            }
            if (node.Right != null){
                Print(node.Right,(Task==false) ? false : true );
            }
        }
    }
 
}
            
void main (string[] args){

    var t = new BinaryTree();

    t.In(8);t.In(3);t.In(10);t.In(1);t.In(6);t.In(14);t.In(4);t.In(7);t.In(13);
    BinaryTreeExtensions.Print(t,true);
    print("\n");
    BinaryTreeExtensions.Print(t,false);

    
}
            
