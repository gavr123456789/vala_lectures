using Gee;

void main () {
    var map = new HashMap<int, int> ();
    int []arr = {4,2, 1, 2, 5, 5, 6, 5, 7, 8, 7};
    int j=0;
    for (int i=0;i<arr.length;i++) {
        if (!map.has_key(arr[i])) {
            map[arr[i]]=j; j++;
        }
    } print("\n");

    int[] result = new int[map.size];
    //заполнение нового массива
    foreach (var entry in map.entries) {
        stdout.printf ("%d => %d\n", entry.key, entry.value);
        result[entry.value]=entry.key;
    }
    //вывод результата
    foreach (int el in result) {
        print(@"$el\n");
    }
    stdout.printf(@"Элементов в новом массиве: $(result.length)");
}

/*Вывод:
1 => 1
2 => 0
5 => 2
6 => 3
7 => 4
8 => 5

2
1
5
6
7
8
Элементов в новом массиве: 6⏎ 
*/