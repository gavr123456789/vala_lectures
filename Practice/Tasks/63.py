def maxsum(iterable):
    maxsofar = maxendinghere = 0
    for x in iterable:
        maxendinghere = max(maxendinghere + x, 0)
        maxsofar = max(maxsofar, maxendinghere)
    return maxsofar

print(maxsum([-1,10,-9,5,6,-10])