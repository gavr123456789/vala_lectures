using Gee;

void fill_col(ref int[,] arr,int a){
    for (int i = 0; i < arr.length[0]; i++) {
        arr[i,a]=0;
    }
}

void fill_row(ref int[,] arr,int a){
    for (int i = 0; i < arr.length[1]; i++) {
        arr[a,i]=0;
    }
}
//Не требуется для алгоритма
void printMass(int[,] arr){
    print("\n");
    for (int i = 0; i < arr.length[0]; i++) {
        for (int j = 0; j < arr.length[1]; j++) {
            print(@"$(arr[i,j])");
        }
        print("\n");
    }
}
//Не требуется для алгоритма
void generate_Mass(ref int[,] arr){
    for (int i = 0; i < arr.length[0]; i++) {
        for (int j = 0; j < arr.length[1]; j++) {
            if(Random.int_range(1,11)<2)
                arr[i,j]=0;
            else arr[i,j]=1;
            print(@"$(arr[i,j])");
        }
        print("\n");
    }
}

void main(string[] args)
{
	int n = 100000;
    int m = 10000;
    var cols = new HashSet<int> ();
    var rows = new HashSet<int> ();
    int[,] arr = new int[m, n];

    generate_Mass(ref arr);print("\n");

	double seconds=0;ulong microseconds=0;
    Timer timer = new Timer ();
    for (int i = 0; i < arr.length[0]; i++) {
        for (int j = 0; j < arr.length[1]; j++) {
            if(arr[i,j]==0){
                cols.add(j);
                rows.add(i);
            }
        }
    }
    foreach (int s in cols) 
        fill_col(ref arr,s);
    
    foreach (int s in rows) 
        fill_row(ref arr,s);        
    
    timer.stop ();//Замеряю время
    seconds = timer.elapsed (out microseconds);
    
    //  printMass(arr);
    print (@"sec $seconds,microsec $microseconds\n");

}