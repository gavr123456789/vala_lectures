string modify(string str,int n, char chr, char[] delimiters){
var builder = new StringBuilder(str);
int index = 0;
do {
		index = str.index_of_char (delimiters[0], index);
		print (@"$index ");
		builder.data[n+index]=chr;
	} while (index++ >= 0);

return builder.str;
}

void main(string[] args) {
string str = """Lorem ipsum dolor sit amet, consectetur adipiscing elit.""";
char[] delimiters ={' ', ',', '.', '?', '!'};

print("\n"+modify(str,2,'%',delimiters));
}