int maxSum (int[] arr){
    var max1=0,max2=0;
    foreach(var a in arr){
        max1 = int.max(max1+a,0); print(@"max1 = $max1\t");
        max2 = int.max(max2, max1);print(@"max2 = $max2\n");
    }
    return max2; 
}
void main() {
   int[] tor={-1,10,-9,5,6,-10};
   print (@"result = $(maxSum(tor))"); 
}
