using Gtk;

public class CustomWidget : DrawingArea {
    public string text;
    public int newPoint;
    public int[] points;

    public CustomWidget () {
        set_size_request (get_allocated_width (), get_allocated_height ());
        points = new int [10];
        int height = get_allocated_height ();print (@"полученная высота в начале $height");
        for(int i=0;i<10;i++)points[i]=0;
        //this.newPoint = txt;
    }
   // public void setNewPoint(int y) {this.newPoint=y;}
    /* Widget is asked to draw itself */

    public override bool draw (Cairo.Context cr) {
        int width = get_allocated_width ();
        int height = get_allocated_height ();
        print ("ширина = %d, высота = %d \n",width,height);
        //цикл в котором отрисовываем массив points[j] с шагом в 1/10 экрана
        for(int i=0,j=0 ;i<width-width / 10 ;i+=width / 10, j++) {
            cr.line_to(i,height-((height/10) *points[j]));
            print("j = %d ",j);
            print(" x %d, y %d \n",i,height/10 *points[j]);
        }
       cr.line_to(10* width / 10,height-newPoint*height/10);
       print(" x %d, y %d \n",10* width/10,newPoint*height/10);
       cr.stroke (); 
    return false;
    }
    public  bool update(int y){
        this.newPoint=y;
        for(int i=0;i<10-1;i++) {
            points[i]=points[i+1];
        } 
        points[9]=y;

        redraw_canvas ();
            return true; 
    }
    private void redraw_canvas () {
        var window = get_window ();
        if (null == window) {
            return;
        }
        var region = window.get_clip_region ();
        // redraw the cairo canvas completely by exposing it
        window.invalidate_region (region, true);
        window.process_updates (true);
    }

 
   
}