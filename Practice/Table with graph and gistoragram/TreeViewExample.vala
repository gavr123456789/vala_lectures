using Gtk;
public class Application : Gtk.Window {
    public Gtk.LevelBar[] barMass;
    private bool switchFlag=false;
    private bool switchFlag2=false;
    private int t=0;
    private int gotovnostProc=0;
    private Gtk.LevelBar bar;
    private Gtk.Label label;
    private Gtk.ListStore list;  
    private Gtk.ListStore list2;
    private Gtk.TreeIter iter;//строка в таблице
    private Gtk.TreeIter iter2;//строка в таблице
    private Gtk.Switch switcher2;
    public Gtk.Frame box3;
    private Pango.Layout layout;
    private CustomWidget widget;
    public Application () {
        this.title = "My Gtk.TreeView";
        this.window_position = Gtk.WindowPosition.CENTER;
        this.destroy.connect (Gtk.main_quit);
        this.set_default_size (350, 70);
        var header = new Gtk.HeaderBar();
        header.set_show_close_button(true);
        this.set_titlebar(header);
        widget = new CustomWidget ();
        // переменные
        barMass = new Gtk.LevelBar[10];
        //drawArea = new  Gtk.Widget ();
        list  = new Gtk.ListStore (2, typeof (int), typeof (bool));
        list2 = new Gtk.ListStore (2, typeof (int), typeof (bool));   
        switcher2 = new Gtk.Switch(); 
        box3 = new Gtk.Frame("График");  
        var switcher = new Gtk.Switch ();
        var box = new Gtk.Box(VERTICAL,2);
        var box2 = new Gtk.Box(Gtk.Orientation.VERTICAL, 1);
        var box1 = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 1);
            label = new Gtk.Label("Тут будет первая банка");
        var button = new Gtk.Button.with_label("DELETE");
        
        //создаем 10 прогрессБаров и заполняем ими бокс с флагами expand
        for (int i=0;i<10;i++){
            barMass[i] = new Gtk.LevelBar.for_interval (0.0, 10.0);
            box1.pack_start(barMass[i],true,true,2);
            barMass[i].set_orientation(Gtk.Orientation.VERTICAL);
            //barMass[i].set_mode (Gtk.LevelBarMode.DISCRETE);
            barMass[i].set_inverted(true);
            barMass[i].set_hexpand(true);
        }

        //Стак
            var stack = new Gtk.Stack();
            var stackSwitcher = new Gtk.StackSwitcher ();
            stackSwitcher.set_valign (Gtk.Align.CENTER);
            stack.set_transition_duration (500);//скорость анимации
            stack.set_transition_type (SLIDE_LEFT_RIGHT);
            stackSwitcher.set_stack (stack);//устанавливаем переключателю стак
            header.pack_start (stackSwitcher);
            stack.add_titled(box1,"box1","box1");
            stack.add_titled(box,"box2","box2");
            stack.add_titled(box3,"box3","box3");

        //бар
        bar = new Gtk.LevelBar.for_interval (0.0, 10.0);
        bar.set_mode (Gtk.LevelBarMode.DISCRETE);
        

        //добавление в лист элементов
            //  list.append (out iter);
            //  list.set (iter, 0, Random.int_range (1, 10), 1, false);//итер, номер столбца, значение столбца и тд
            //  list.append (out iter);
            //  list.set (iter, 0, Random.int_range (1, 10), 1, false);

            //  list2.append (out iter2);
            //  list2.set(iter2, 0, Random.int_range (1, 10), 1, true);
            //  list2.append (out iter2);
            //  list2.set(iter2, 0, Random.int_range (1, 10), 1, true);

        //коннект свича к таймеру
        switcher.notify["active"].connect (() => {
			if (switcher.active) {
				print ("Завод запущен!\n");
				switchFlag=true;
				process_add();
			} else {
			    print ("Завод остановлен\n");
                switchFlag=false;
                t=0;bar.set_value (t);
			}
                });
        switcher2.notify["active"].connect (() => {
            if (switcher2.active) {
                print ("Процесс обработки начался!\n");
                switchFlag2=true;
                process();
            } else {
                print ("Процесс обработки закончился!\n");
                switchFlag2=false;
                gotovnostProc=0;bar.set_value (0);
                process();
            }
                });  

        // The View:
            Gtk.TreeView view  = new Gtk.TreeView.with_model (list);
            Gtk.TreeView view2 = new Gtk.TreeView.with_model (list2);   
            Gtk.CellRendererText cell = new Gtk.CellRendererText ();
            Gtk.CellRendererText cell2 = new Gtk.CellRendererText ();

            var column_1 = new Gtk.TreeViewColumn.with_attributes( "№ банки", cell, "text",  0);
            var column_2 = new Gtk.TreeViewColumn.with_attributes( "% кофя", cell, "text", 1);
            var column_3 = new Gtk.TreeViewColumn.with_attributes( "№ банки", cell2, "text", 0);
            var column_4 = new Gtk.TreeViewColumn.with_attributes( "% кофя", cell2, "text", 1);
            column_1.set_sort_column_id(0); column_2.set_sort_column_id(1);
            column_3.set_sort_column_id(0); column_4.set_sort_column_id(1);
            view.append_column(column_1); view.append_column(column_2);
            view2.append_column(column_3); view2.append_column(column_4);

            button.clicked.connect (() => {
            deleteEl();
            });

    //view.insert_column_with_attributes (-1, "№ банки", cell, "text", 0); // на память о легком способе
        box.add(switcher);
        box.add(button);
        box.add (view);
        box.add(switcher2);
        box.add(label);
        box.add(bar);
        box.add(view2);

        box3.add(widget);
        this.add(stack);
    }
    private bool process_add(){
        if(switchFlag==true && t<10){
            int temp = Random.int_range (1, 10);
            Timeout.add(Random.int_range (2000, 4000), process_add);
            addNbar((double)temp);
            print (@"$(t++) \n");
            list.append (out iter);
            list.set (iter, 0, temp, 1, false);
            }
        return false;
    }

	private bool process(){
	    print(@"флаг2 = $(switchFlag2) готовность=$(gotovnostProc)");
        if(switchFlag2==true && gotovnostProc<10){
            print (@"Готовность % $(gotovnostProc++) \n");
            bar.set_value (gotovnostProc);//Изменения прогресс бара
            Gtk.TreeIter iter;
            if(list.get_iter_first (out iter) || gotovnostProc<=10){
                Timeout.add(Random.int_range (500, 700), process); print("Время добавлено\n");
            }
            if(gotovnostProc==1 ){
                int name;
                name = deleteEl();
                list2.append (out iter);//добавление нового элемента во второй список
                list2.set (iter, 0,name, 1, true);
                t--;
            }
            if(gotovnostProc==10)gotovnostProc=0;
        }
        return false;
    }

    public void addNbar(double newV){
        for(int i=0;i<10-1;i++) {
            barMass[i].set_value(barMass[i+1].get_value());
        }
        barMass[9].set_value(newV);   
        widget.update((int) newV);
    }


    private int deleteEl(){
        Gtk.TreeIter iter;
        GLib.Value name;
        bool next = list.get_iter_first (out iter);//если у нас в списке еще чето есть(out не требует обязательного наличия)
        if(next){
            list.get_value(iter,0, out name);
            int temp = (int)name; 
            label.set_text ("Номер банки " + temp.to_string());
            print( @"номер банки $temp");
            list.remove(ref iter);//ref требует обязательного наличия
            return(temp);
        }else{
            switcher2.set_active(false); 
            switchFlag2=false;
        }
	return 0;
    }

    public static int main (string[] args) {
        Gtk.init (ref args);
        Application app = new Application ();
        //var widget = new CustomWidget ();
       // app.box3.add(widget);
        app.show_all ();
        Gtk.main ();
        return 0;
    }
}

public class CustomWidget : DrawingArea {
    public string text;
    public int newPoint;
    public int[] points;

    public CustomWidget () {
        set_size_request (get_allocated_width (), get_allocated_height ());
        points = new int [10];
        int height = get_allocated_height ();print (@"полученная высота в начале $height");
        for(int i=0;i<10;i++)points[i]=0;
        //this.newPoint = txt;
    }
   // public void setNewPoint(int y) {this.newPoint=y;}
    /* Widget is asked to draw itself */

    public override bool draw (Cairo.Context cr) {
        int width = get_allocated_width ();
        int height = get_allocated_height ();
        print ("ширина = %d, высота = %d \n",width,height);
        //цикл в котором отрисовываем массив points[j] с шагом в 1/10 экрана
        for(int i=0,j=0 ;i<width-width / 10 ;i+=width / 10, j++) {
            cr.line_to(i,height-((height/10) *points[j]));
            print("j = %d ",j);
            print(" x %d, y %d \n",i,height/10 *points[j]);
        }
       cr.line_to(10* width / 10,height-newPoint*height/10);
       print(" x %d, y %d \n",10* width/10,newPoint*height/10);
       cr.stroke (); 
    return false;
    }
    public  bool update(int y){
        this.newPoint=y;
        for(int i=0;i<10-1;i++) {
            points[i]=points[i+1];
        } 
        points[9]=y;

        redraw_canvas ();
            return true; 
    }
    private void redraw_canvas () {
        var window = get_window ();
        if (null == window) {
            return;
        }
        var region = window.get_clip_region ();
        // redraw the cairo canvas completely by exposing it
        window.invalidate_region (region, true);
        window.process_updates (true);
    }

 
   
}
