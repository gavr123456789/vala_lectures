using Gtk;

public class StackClass : Gtk.Window {
	public Gtk.LevelBar[] barMass;
	public Gtk.Frame box3;
	public int[] mainMass;
	private CustomWidget widget;
	public StackClass () {
		 //Header
		var header = new Gtk.HeaderBar();
		header.set_show_close_button(true);
		barMass = new Gtk.LevelBar[10];
		mainMass = new int[10];
		box3 = new Gtk.Frame("График");  
		widget = new CustomWidget ();

 
		var box1 = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 1);
		var label = new Label("result");
		header.pack_end(label);

 //Стак
		 var stack = new Gtk.Stack();
		 Gtk.StackSwitcher switcher = new Gtk.StackSwitcher ();
		 switcher.set_valign (Gtk.Align.CENTER);
		 header.pack_start (switcher);

		 stack.set_transition_duration (500);//скорость анимации
		 stack.set_transition_type (SLIDE_LEFT_RIGHT);
		 switcher.set_stack (stack);//устанавливаем переключателю стак
 //создаем 10 прогрессБаров и заполняем ими бокс с флагами expand
		 for (int i=0;i<10;i++){
			 barMass[i] = new Gtk.LevelBar.for_interval (0.0, 10.0);
			 box1.pack_start(barMass[i],true,true,2);
			 barMass[i].set_orientation(Gtk.Orientation.VERTICAL);
			 //barMass[i].set_mode (Gtk.LevelBarMode.DISCRETE);
			 barMass[i].set_inverted(true);
			 barMass[i].set_hexpand(true);
		 }
 
		 box3.add(widget);
		 barMass[0].set_value(10.0);
	 
		 stack.add_titled(box1,"box1","box1");
		 stack.add_titled(box3,"box3","box3");
		 
 
		 this.set_titlebar(header);
		 this.destroy.connect(Gtk.main_quit);
		 this.border_width = 10;
		 this.set_size_request (600, 500);
		 this.add(stack);
 
		 this.key_press_event.connect ((key) => {
			print(key.str + " ");
			addNbar(double.parse(key.str));
			label.set_text( sas(mainMass));
			return false;
		 });
 
	 }
 
 
	 public void addNbar(double newV){
		for(int i=0;i<10-1;i++)
			barMass[i].set_value(barMass[i+1].get_value());
		 
		barMass[9].set_value(newV);  
		widget.update((int) newV);
		for(int i=0;i<10;i++) mainMass[i]=(int)barMass[i].get_value();
	 }
 
 
	 public static int main(string[] args){
		 Gtk.init(ref args);
		 var s = new StackClass();
		 s.show_all();
		 Gtk.main();
		 return 0;
	 }
 }
