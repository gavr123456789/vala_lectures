using Gtk;

void buffer1_changed (){
	string s = input.buffer.text;
	StringBuilder sb = new StringBuilder();
	int count = 1;
	char current =s[0];
	for(int i = 1; i < s.length;i++){
		if (current == s[i])
			count++;
		else{
			sb.append(@"$count$current");
			count = 1;
			current = s[i];
		}
	}
	sb.append(@"$count$current");

	output.buffer.text = sb.str;

}

void on_textbuffer2_changed (){
	string s = output.buffer.text;
	string a = "";
           int count = 0;
           StringBuilder sb = new StringBuilder();
           char current ;
           for(int i = 0; i < s.length; i++){
               current = s[i];
               if (current.isdigit())
                   a += current.to_string();
               else{
                   count = int.parse(a);
                   a = "";
                   for (int j = 0; j < count; j++)
                       sb.append(current.to_string());
               }
		   }
	input.buffer.text = sb.str;
	print(@"$(sb.str)\n");
		   
}

TextView input;
TextView output;
public class Sas {

	public static TextIter end;
	public static Gtk.Clipboard clipboard;
	public Sas () {
		var builder = new Builder();
		builder.add_from_file("ui.ui");
		builder.connect_signals (null);
		var window = builder.get_object("window") as Window;
		window.destroy.connect(Gtk.main_quit);
		window.window_position = WindowPosition.CENTER;
		input = builder.get_object("input") as TextView;
		output = builder.get_object("output") as TextView;
		window.show_all ();
	}

	public static int main (string[] args) {
		Gtk.init (ref args);
		var app = new Sas ();
		Gtk.main ();
		return 0;
	}
}
